const Discord = require('discord.js');
const fs = require('fs');
const schedule = require('node-schedule');
const Scrape= require('./modules/scrape');

let bot = new Discord.Client();

const formatScrape = res => {
  return bot.channels.get('235045844892516353').send(res.menus.map(place => `\n${place.name}:
<${place.link}>`),
  {
    files: [
      `./screenshots/${res.fileName}`
    ]
  }).then(() => fs.unlink(`./screenshots/${res.fileName}`).then(() => {}));

}

const scrapeResult= () => {
  Scrape.scrape().then(res => {
    if(!res) return;
    formatScrape(res);
  });
};

const j = schedule.scheduleJob('*/5 * * * *', function(){
  scrapeResult();
});

bot.on('ready', () => {
  console.log('logged in');
});

bot.on('message', message => {
  const msg= message;
  if(message.content.toLowerCase() === '!menu') {
    scrapeResult()
  }
});

bot.login('KEY');
