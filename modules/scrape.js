const puppeteer = require('puppeteer');

class Scrape {
  constructor() {
    this.menuList= [];
  }

  async scrape() {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const fileName= `${Date.now()}.png`;

    await page.goto('https://www.scribd.com/user/346294009/Ashok-Selvam/uploads');
    await page.setViewport({width: 1680, height: 930});

    const menus= await page.$$eval('.doc_link', nodes => nodes.slice(0,6).map(n => {
      return {
        // is there a better way to do this selector?
        name: n.children[1].children[0].innerText,
        link: n.href
      }
    }));
    const titles= menus.map(i => i.name);

    const areEqual = this.menuList.length === titles.length && this.menuList.every((item, index) => titles[index] === item);
    this.menuList= titles;

    // don't do anything if they're equal
    if(areEqual) {
      console.log('no change happened');
      await browser.close();
      return false;
    }

    // get a screenshot of the page
    await page.screenshot({path: `screenshots/${fileName}`});

    // close out
    await browser.close();
    return {
      menus,
      fileName
    };
  };
}

module.exports = new Scrape();
